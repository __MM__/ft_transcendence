## General

The aim of this project was to construct a __fullstack webapp__ based on the classic first-ever videogame : __Pong__. 

The game, is really just a pretext to make us work on everything that revolves around the build of a webapp, such as :  

- establishing an architecture 🏗️
- code as a group 🧑🏻‍💻👨‍💻👨🏻‍💻🧑🏽‍💻
- receive and create complex HTTP requests and responses
- manage and edit a database
- connect users through websockets
- secure access to site and datas in database ⚔️
- and many more...

## Try it Yourself

1. Clone this repository with `git clone git@gitlab.com:__MM__/ft_transcendence.git`
1. Make sure you have `docker` installed on your machine
2. run the command `docker-compose up --build`
3. after a few seconds launch your browser and type [http://localhost:4000](http://localhost:4000) to access the WebApp. It might need a little time to deploy, so don't hesitate to refresh the page.
4. enjoy it !

## Features

- **Multiplayer mode** : play in real time against other players online in a Pong game, thanks to websockets. [Socket.io](http://socket.io/).
- **SSO Auth** : connect to your 42 account and access your game stats and your profile parameters. 2FA is available.
- **Instant chat** : chat with other players in a private message or in channels with multiple users.
- **Game profile** : check your game stats, such as number of played games, victory rate, E.L.O score, ranking against other players, using a PostgreSQL database to store those datas.
